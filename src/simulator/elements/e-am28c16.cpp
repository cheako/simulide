/***************************************************************************
 *   Copyright (C) 2019 by Mike Mestnik                                    *
 *   cheako+copyright_simulide@mikemestnik.net                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <http://www.gnu.org/licenses/>.  *
 *                                                                         *
 ***************************************************************************/

#include "e-am28c16.h"
#include "e-node.h"
// #include <sstream>
#include <cmath>

eAm28c16::eAm28c16(std::string id)
    : eLogicDevice(id)
{
}
eAm28c16::~eAm28c16()
{
}

void eAm28c16::initialize()
{
    for (int i = 8; i < 13; i++) // Initialize control pins
    {
        eNode *enode = m_input[i]->getEpin()->getEnode();
        if (enode)
            enode->addToChangedFast(this);
    }
    for (int i = 0; i < 8; i++)
        m_dataPinState[i] = false;

    m_e = false;
    m_g = false;

    eLogicDevice::initialize();

    double imp = 1e28;
    for (int i = 0; i < m_numOutputs; i++)
        m_output[i]->setImp(imp);
}

void eAm28c16::setVChanged()
{
    bool E = eLogicDevice::getInputState(13);
    if (E != m_e)
    {
        m_e = E;

        if (!E)
        {
            m_g = false;
            double imp = 1e28;
            for (int i = 0; i < m_numOutputs; i++)
                m_output[i]->setImp(imp);
        }
    }
    if (!E)
        return;

    bool G = eLogicDevice::getInputState(14);
    bool g = eLogicDevice::outputEnabled() && !G;

    if (g != m_g)
    {
        m_g = g;
        eLogicDevice::setOutputEnabled(g);
    }
    int address = 0;

    for (int i = 0; i < 11; i++) // Get Address
    {
        bool state = eLogicDevice::getInputState(i);

        if (state)
            address += pow(2, 7 - i);
    }
    if (G) // Write
    {
        if (eLogicDevice::getInputState(15))
        {
            int value = 0;

            for (int i = 0; i < 8; i++)
            {
                int volt = m_output[i]->getEpin()->getVolt();

                bool state = m_dataPinState[i];

                if (volt > m_inputHighV)
                    state = true;
                else if (volt < m_inputLowV)
                    state = false;

                m_dataPinState[i] = state;
                //qDebug() << "Bit " << i << state;
                if (state)
                    value += pow(2, i);
            }
            m_flash[address] = value;
            //qDebug() << "Writting " << address << value;
        }
    }
    else // Read
    {
        int value = m_flash[address];
        //qDebug() << "Reading " << address << value;
        for (int i = 0; i < 8; i++)
        {
            bool pinState = value & 1;
            m_output[i]->setOut(pinState);
            m_output[i]->stampOutput();
            //qDebug() << "Bit " << i << pinState;
            value >>= 1;
        }
    }
}
