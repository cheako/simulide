/***************************************************************************
 *   Copyright (C) 2019 by Mike Mestnik                                    *
 *   cheako+copyright_simulide@mikemestnik.net                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <http://www.gnu.org/licenses/>.  *
 *                                                                         *
 ***************************************************************************/

#ifndef EAM28C16_H
#define EAM28C16_H

#include "e-logic_device.h"

class MAINMODULE_EXPORT eAm28c16 : public eLogicDevice
{
    public:

        eAm28c16( std::string id );
        ~eAm28c16();
        
        virtual void initialize();
        virtual void setVChanged();
        
    protected:
        char unsigned m_flash[2<<12];
        bool m_dataPinState[8];
        
        bool m_e;
        bool m_g;
};


#endif
