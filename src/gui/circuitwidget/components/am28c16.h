/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2016 by santiago González                               *
 *   santigoro@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef AM26C16_H
#define AM26C16_H

#include "itemlibrary.h"
#include "logiccomponent.h"
#include "e-am28c16.h"

class MAINMODULE_EXPORT Am28c16 : public LogicComponent, public eAm28c16
{
    Q_OBJECT

    public:

        Am28c16( QObject* parent, QString type, QString id );
        ~Am28c16();
        
        static Component* construct( QObject* parent, QString type, QString id );
        static LibraryItem *libraryItem();
};

#endif
