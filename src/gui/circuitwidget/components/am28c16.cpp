/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2019 by Mike Mestnik                                    *
 *   cheako+copyright_simulide@mikemestnik.net                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "am28c16.h"
#include "e-source.h"
#include "itemlibrary.h"
#include "connector.h"
#include "pin.h"


Component* Am28c16::construct( QObject* parent, QString type, QString id )
{
        return new Am28c16( parent, type, id );
}

LibraryItem* Am28c16::libraryItem()
{
    return new LibraryItem(
        tr( "AM28C16" ),
        tr( "Logic" ),
        "subc.png",
        "Am28c16",
        Am28c16::construct );
}

Am28c16::Am28c16( QObject* parent, QString type, QString id )
     : LogicComponent( parent, type, id )
     , eAm28c16( id.toStdString() )
{
    m_width  = 4;
    m_height = 12;
    
    QStringList pinList;                              // Create Pin List

    pinList // Inputs:
            << "IL08 A0"
            << "IL07 A1"
            << "IL06 A2"
            << "IL05 A3"
            << "IL04 A4"
            << "IL03 A5"
            << "IL02 A6"
            << "IL01 A7"
            << "IR01 A8"
            << "IR02 A9"
            << "IR05 A10"
            
            << "IR06 E"
            << "IR04 G"
            << "IR03 W"
            
            // Outputs:
            << "OL09 D0"
            << "OL10 D1"
            << "OL11 D2"
            << "OR11 D3"
            << "OR10 D4"
            << "OR09 D5"
            << "OR08 D6"
            << "OR07 D7"
            ;
    init( pinList );                   // Create Pins Defined in pinList
     
    for( int i=0; i<8; i++ ) 
    {
        eLogicDevice::createInput( m_inPin[i] );
        eLogicDevice::createOutput( m_outPin[i] );
    }
    for( int i=8; i<11; i++ ) 
    { 
        eLogicDevice::createInput( m_inPin[i] );
    }
    eLogicDevice::createInput( m_inPin[11] );                       // E
    eLogicDevice::createOutEnablePin( m_inPin[12] );                // G
    eLogicDevice::createInput( m_inPin[13] );                       // W
    
    for( int i=8; i<13; i++ ) m_input[i]->setInverted( true ); // Invert control pins    m_area = QRect( 0, 0, 8*4, 8*5 );
}

Am28c16::~Am28c16()
{
}
